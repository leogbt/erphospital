<?php
class Conexao {
    
	private $host = "localhost";
	private $usuario = "root";
	private $senha = "";
	private $banco = "bd_artigo_mvc_ioc";

	public function execute() {
		try{
			$pdo = new PDO(
				'mysql:host=' . $this->host . ';
				dbname=' . $this->banco . '', 
				$this->usuario, 
				$this->senha
			);
			
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return $pdo;
			
		}catch(Exception $e){
		    echo $e->getMessage();
            exit();
		}
	}  
}
?>