<?php
class Contrato {
	
    /*
     * Utilizamos aqui o padr�o de projeto Value Object, 
     * juntamente com o padr�o de projeto Encapsulamento de Dados.
     * 
     * Value Object: 
     * Objeto cujo sua unica finalidade �
     * armazenar valores.
     * 
     * Encapsulamento de dados: 
     * Os atributos s�o todos privados, para
     * acessa-los utiliza-se metodos publicos.
     */
    
    private $codigo;
	private $empresa;
    private $departamento;
    private $dataInicio;
	private $tipoContrato;		
	private $valor;
    private $processoLicitacao;
    private $objetoContrato;
	private $gestor;
	private $dataFim;
    private $status;

	
     
    public function getCodigo(){
        return $this->codigo;
    }
    
    public function setCodigo($codigo){
        $this->codigo = $codigo;
    }
    
    public function getEmpresa(){
        return $this->empresa;
    }
    
    public function setEmpresa($empresa){
        $this->empresa = $empresa;
    }
    
    public function getDepartamento(){
        return $this->departamento;
    }
    
    public function setDepartamento($departamento){
        $this->departamento = $departamento;
    }
	
	public function getDataInicio(){
        return $this->dataInicio;
    }
    
    public function setDataInicio($dataInicio){
        $this->dataInicio = $dataInicio;
    }

	public function getTipoContrato(){
        return $this->tipoContrato;
    }
    
    public function setTipoContrato($tipoContrato){
        $this->tipoContrato = $tipoContrato;
    }	
	
	public function getValor(){
        return $this->valor;
    }
    
    public function setValor($valor){
        $this->valor = $valor;
    }

	public function getProcessoLicitacao(){
        return $this->processoLicitacao;
    }
    
    public function setProcessoLicitacao($processoLicitacao){
        $this->processoLicitacao = $processoLicitacao;
    }

	public function getObjetoContrato(){
        return $this->objetoContrato
    }
    
    public function setObjetoContrato($objetoContrato){
        $this->objetoContrato = $objetoContrato;
    }
	
	public function getGestor(){
        return $this->gestor;
    }
    
    public function setGestor($gestor){
        $this->gestor = $gestor;
    }
	
	public function getDataFim()){
        return $this->dataFim;
    }
    
    public function setDataFim($dataFim){
        $this->dataFim = $dataFim;
    }
	
	public function getStatus(){
        return $this->status;
    }
    
    public function setStatus($status){
        $this->status = $status;
    }
	
	
    
}

?>
