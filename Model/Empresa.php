<?php
class Empresa {
	
    /*
     * Utilizamos aqui o padr�o de projeto Value Object, 
     * juntamente com o padr�o de projeto Encapsulamento de Dados.
     * 
     * Value Object: 
     * Objeto cujo sua unica finalidade �
     * armazenar valores.
     * 
     * Encapsulamento de dados: 
     * Os atributos s�o todos privados, para
     * acessa-los utiliza-se metodos publicos.
     */
    
    private $cnpj;
    private $razaoSocial;
    private $nomeFantasia;
	private $inscricaoEstadual;
	private $areaAtuacao;
    private $registroANS;
	private $telefone;
	private $bairro;
	private $rua;
	private $cidade;
	private $estado;
	
    public function getCnpj(){
        return $this->cnpj;
    }
    
    public function setCnpj($cnpj){
        $this->cnpj = $cnpj;
    }
    
    public function getRazaoSocial(){
        return $this->razaoSocial;
    }
    
    public function setRazaoSocial($razaoSocial){
        $this->razaoSocial = $razaoSocial;
    }
    
    public function getNomeFantasia(){
        return $this->nomeFantasia;
    }
    
    public function setNomeFantasia($nomeFantasia){
        $this->nomeFantasia = $nomeFantasia;
    }
	
	public function getInscricaoEstadual(){
        return $this->inscricaoEstadual;
    }
    
    public function setInscricaoEstadual($inscricaoEstadual){
        $this->inscricaoEstadual = $inscricaoEstadual;
    }

	public function getAreaAtuacao(){
        return $this->areaAtuacao;
    }
    
    public function setAreaAtuacao($areaAtuacao){
        $this->areaAtuacao = $areaAtuacao;
    }	
	
	public function getRegistroANS(){
        return $this->registroANS;
    }
    
    public function setRegistroANS($registroANS){
        $this->registroANS = $registroANS;
    }

	public function getTelefone(){
        return $this->telefone;
    }
    
    public function setTelefone($telefone){
        $this->telefone = $telefone;
    }

	public function getBairro(){
        return $this->bairro
    }
    
    public function setBairro($bairro){
        $this->bairro = $bairro;
    }
	
	public function getRua(){
        return $this->rua;
    }
    
    public function setRua($rua){
        $this->rua = $rua;
    }
	
	public function getCidade(){
        return $this->cidade;
    }
    
    public function setCidade($cidade){
        $this->cidade = $cidade;
    }
	
	public function getEstado(){
        return $this->estado;
    }
    
    public function setEstado($estado){
        $this->estado = $estado;
    }		
}

?>
