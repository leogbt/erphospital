<?php
class Departamento {
	
    /*
     * Utilizamos aqui o padr�o de projeto Value Object, 
     * juntamente com o padr�o de projeto Encapsulamento de Dados.
     * 
     * Value Object: 
     * Objeto cujo sua unica finalidade �
     * armazenar valores.
     * 
     * Encapsulamento de dados: 
     * Os atributos s�o todos privados, para
     * acessa-los utiliza-se metodos publicos.
     */
    
    private $codigo;
    
    private $nome;
    
    private $responsavel;
     
    public function getCodigo(){
        return $this->codigo;
    }
    
    public function setCodigo($codigo){
        $this->codigo = $codigo;
    }
    
    public function getNome(){
        return $this->nome;
    }
    
    public function setNome($nome){
        $this->nome = $nome;
    }
    
    public function getResponsavel(){
        return $this->responsavel;
    }
    
    public function setResponsavel($responsavel){
        $this->responsavel = $responsavel;
    }

}

?>
