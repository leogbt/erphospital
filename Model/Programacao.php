<?php
class Programacao {
	
    /*
     * Utilizamos aqui o padr�o de projeto Value Object, 
     * juntamente com o padr�o de projeto Encapsulamento de Dados.
     * 
     * Value Object: 
     * Objeto cujo sua unica finalidade �
     * armazenar valores.
     * 
     * Encapsulamento de dados: 
     * Os atributos s�o todos privados, para
     * acessa-los utiliza-se metodos publicos.
     */
    
    private $codigo;
    private $contrato;
    private $tipoProgramacao;
	private $dataVencimento;
	private $descricao;
	private $valor;
     
    public function getCodigo(){
        return $this->codigo;
    }
    
    public function setCodigo($codigo){
        $this->codigo = $codigo;
    }
    
    public function getContrato(){
        return $this->contrato;
    }
    
    public function setContrato($contrato){
        $this->contrato = $contrato;
    }
    
    public function getTipoProgramacao(){
        return $this->tipoProgramacao;
    }
    
    public function setTipoProgramacao($tipoProgramacao){
        $this->tipoProgramacao = $tipoProgramacao;
    }

    public function getDataVencimento(){
        return $this->dataVencimento;
    }
    
    public function setDataVencimento($dataVencimento){
        $this->dataVencimento = $dataVencimento;
    }	

	public function getDescricao(){
        return $this->descricao;
    }
    
    public function setDescricao($descricao){
        $this->descricao = $descricao;
    }

	public function getValor(){
        return $this->valor;
    }
    
    public function setValor($valor){
        $this->valor = $valor;
    }	
    
}

?>
