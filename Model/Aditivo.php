<?php
class Aditivo {
	
    /*
     * Utilizamos aqui o padr�o de projeto Value Object, 
     * juntamente com o padr�o de projeto Encapsulamento de Dados.
     * 
     * Value Object: 
     * Objeto cujo sua unica finalidade �
     * armazenar valores.
     * 
     * Encapsulamento de dados: 
     * Os atributos s�o todos privados, para
     * acessa-los utiliza-se metodos publicos.
     */
    
    private $codigo;
    
    private $contrato;
    
    private $dataAditivo;
	
	private $valor;
     
    public function getCodigo(){
        return $this->codigo;
    }
    
    public function setCodigo($codigo){
        $this->codigo = $codigo;
    }
    
    public function getContrato(){
        return $this->contrato;
    }
    
    public function setContrato($contrato){
        $this->contrato = $contrato;
    }
    
    public function getDataAditivo(){
        return $this->dataAditivo;
    }
    
    public function setDataAditivo($dataAditivo){
        $this->dataAditivo = $dataAditivo;
    }
	
	public function getValor(){
        return $this->valor;
    }
    
    public function setValor($valor){
        $this->valor = $valor;
    }
	
}

?>
