<?php
class Reajuste {
	
    /*
     * Utilizamos aqui o padr�o de projeto Value Object, 
     * juntamente com o padr�o de projeto Encapsulamento de Dados.
     * 
     * Value Object: 
     * Objeto cujo sua unica finalidade �
     * armazenar valores.
     * 
     * Encapsulamento de dados: 
     * Os atributos s�o todos privados, para
     * acessa-los utiliza-se metodos publicos.
     */
    
    private $codigo;
    
    private $contrato;
    
    private $dataReajuste;
	
	private $indice;
	
	private $descricao;
     
    public function getCodigo(){
        return $this->codigo;
    }
    
    public function setCodigo($codigo){
        $this->codigo = $codigo;
    }
    
    public function getContrato(){
        return $this->contrato;
    }
    
    public function setContrato($contrato){
        $this->contrato = $contrato;
    }
    
    public function getDataReajuste(){
        return $this->dataReajuste;
    }
    
    public function setDataReajuste($dataReajuste){
        $this->dataReajuste = $dataReajuste;
    }
	
	public function getIndice(){
        return $this->indice;
    }
    
    public function setIndice($indice){
        $this->indice = $indice;
    }
	
	public function getDescricao(){
        return $this->descricao;
    }
    
    public function setIndice($descricao){
        $this->descricao = $descricao;
    }
	
    
}

?>
